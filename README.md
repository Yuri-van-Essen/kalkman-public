# Kalkman • beddenspecialist.nl

##  Version 2020.1

### List of changes and additions

##### **Date of last modification**

```
13-05-2020
```

##### **Modifications | 13-05-2020**
```
• ACF group 'Pagina 2020' edited
• '/assets/css/' edited
```

##### **Modifications | 03-04-2020**
```
• ACF group 'Pagina 2020' edited
• '/templates/subpage-2020.php' edited 
```


##### **Modifications | 29-03-2020**
```
• Plugin Advanced Custom Fields PRO upgraded to 5.8.9
• Plugin WP Notification Bars deactivated (conflicts)

• ACF Groep created 'Pagina 2020'
• ACF Groep created 'Algemeen: Button'

• Folder added '/assets'
• Folder added '/acf-json'

• Folder + file: 'classes' + class-wp-bootstrap-navwalker.php
• Folder + file: 'templates' + subpage-2020.php

• File created 'header-2020.php'
• File created 'footer-2020.php'

• functions.php custom code from line 727
```

### By

* *[Yuri van Essen](mailto:info@yurivanessen.nl)*